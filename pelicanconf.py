#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Joseph Gruber'
SITENAME = 'Mile High Space'
SITEURL = ''
TIMEZONE = 'Etc/UTC'
DEFAULT_LANG = 'en'

PATH = 'content'
STATIC_PATHS = ['assets']
PAGE_PATHS = ['pages']
ARTICLE_PATHS = ['posts']

OUTPUT_PATH = 'output'
DELETE_OUTPUT_DIRECTORY = True

THEME = 'theme'

DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False

DEFAULT_DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 10

TYPOGRIFY = True
